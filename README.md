# Polaris Exporter

Polaris exporter takes values from a [Polaris](https://github.com/FairwindsOps/polaris) audit for consumption via Prometheus.

Currently only provides values for the overall score and success/warning/danger counts.

!!! Still a work in progress !!!