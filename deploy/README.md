# Polaris Exporter

Polaris exporter takes values from a Polaris audit for consumption via Prometheus.

Allows prometheus to alert on polaris changes

Currently only provides values for the overall score and success/warning/danger counts.